<?php
namespace Vokuro\Controllers;

use Phalcon\Tag,
    Phalcon\Mvc\Model\Criteria,
    Phalcon\Mvc\Model\Query,
    Phalcon\Paginator\Adapter\Model as Paginator,
    Vokuro\Models\Users,
    Vokuro\Models\Musics,
    Vokuro\Models\Finance,
    Vokuro\Models\Tracks,
    Vokuro\Forms\SearchForm;

/**
 * Vokuro\Controllers\ShowController
 * CRUD to manage profiles
 */
class ShowController extends ControllerBase
{

    /**
     * Default action. Set the private (authenticated) layout (layouts/private.volt)
     */
    public function initialize()
    {
        $this->view->setTemplateBefore('private');
    }

    public function indexAction()
    {
        $this->persistent->conditions = null;
        $this->view->form = new SearchForm();
        $this->view->userType = $this->auth->getUser()->profilesId;

        //Add contacts
        if ($this->auth->getUser()->profilesId == 1)
        {
            // exit('Admin');
        }

        if($this->auth->getUser()->profilesId != 3)
        {
            $builder = $this->modelsManager->createBuilder()
                    ->columns('
                        Vokuro\Models\Musics.code,
                        Vokuro\Models\Musics.title,
                        Vokuro\Models\Musics.member_id,
                        Vokuro\Models\Users.name,
                        Vokuro\Models\Finance.r_year,
                        Vokuro\Models\Finance.r_month,
                        Vokuro\Models\Tracks.orderTimes,
                        Vokuro\Models\Tracks.toneID
                    ')
                    ->from('Vokuro\Models\Musics')
                    ->join('Vokuro\Models\Finance', "Vokuro\Models\Musics.code = Vokuro\Models\Finance.id ")
                    ->join('Vokuro\Models\Tracks', "Vokuro\Models\Musics.code = Vokuro\Models\Tracks.toneCode ")
                    ->join('Vokuro\Models\Users', "Vokuro\Models\Musics.member_id = Vokuro\Models\Users.member_id ")
                    ->orderBy('
                        Vokuro\Models\Finance.r_year DESC,
                        Vokuro\Models\Finance.r_month DESC
                    ')
                    ->getQuery()
                    ->execute();
        } else {
            
            $memberId = $this->auth->getIdentity()['memberId'];

            $builder = $this->modelsManager->createBuilder()
                    ->columns('
                        Vokuro\Models\Musics.code,
                        Vokuro\Models\Musics.title,
                        Vokuro\Models\Musics.member_id,
                        Vokuro\Models\Users.name,
                        Vokuro\Models\Finance.r_year,
                        Vokuro\Models\Finance.r_month,
                        Vokuro\Models\Tracks.orderTimes,
                        Vokuro\Models\Tracks.toneID
                    ')
                    ->from('Vokuro\Models\Musics')
                    ->join('Vokuro\Models\Finance', "Vokuro\Models\Musics.code = Vokuro\Models\Finance.id ")
                    ->join('Vokuro\Models\Tracks', "Vokuro\Models\Musics.code = Vokuro\Models\Tracks.toneCode ")
                    ->join('Vokuro\Models\Users', "Vokuro\Models\Musics.member_id = Vokuro\Models\Users.member_id ")
                    ->where("
                        Vokuro\Models\Musics.member_id = '{$memberId}'
                    ")
                    ->orderBy('
                        Vokuro\Models\Finance.r_year DESC,
                        Vokuro\Models\Finance.r_month DESC
                    ')
                    ->getQuery()
                    ->execute();
        }

        $paginator = new Paginator(array(
            "data" => $builder,
            "limit" => 10,
            "page" => ($this->request->getQuery('page'))?$this->request->getQuery('page'):1
        ));

        $this->view->dataList = $paginator->getPaginate();
        $totalPages = array();
        for ( $idx = 1; $idx <= $paginator->getPaginate()->total_pages; $idx++)
        {
            $totalPages[] = $idx;
        }
        $this->view->totalPages = $totalPages;
    }

    public function searchAction()
    {
        $this->view->userType = $this->auth->getUser()->profilesId;
        $this->view->form = new SearchForm();

        $numberPage = 1;
        if ($this->request->isPost()) {
            $q = $this->request->getPost()['q'];
        } else {
            $numberPage = $this->request->getQuery("page", "int");
            $q = $this->request->getQuery("q");
        }
        $q = trim($q);
        if (strlen($q) < 2)
        {
            return $this->response->redirect('show');
        }

        $qM = $q;
        $qY = $q;
        preg_match_all('/[0-9]{4}\/[0-9]+/', $q, $matches);
        if (count($matches[0]))
        {
            preg_match('/[0-9]{4}?\//', $q, $qY);
            preg_match('/\/[0-9]+/', $q, $qM);
            $qY = trim(str_replace('/', '', $qY[0]));
            $qM = trim(str_replace('/', '', $qM[0]));
        }

        if($this->auth->getUser()->profilesId != 3)
        {
            $builder = $this->modelsManager->createBuilder()
                        ->columns('
                            Vokuro\Models\Musics.code,
                            Vokuro\Models\Musics.title,
                            Vokuro\Models\Musics.member_id,
                            Vokuro\Models\Users.name,
                            Vokuro\Models\Finance.r_year,
                            Vokuro\Models\Finance.r_month
                        ')
                        ->from('Vokuro\Models\Musics')
                        ->join('Vokuro\Models\Finance', "Vokuro\Models\Musics.code = Vokuro\Models\Finance.id ")
                        ->join('Vokuro\Models\Users', "Vokuro\Models\Musics.member_id = Vokuro\Models\Users.id ")
                        ->where("
                            Vokuro\Models\Musics.code LIKE :q:
                        ")
                        ->orWhere("
                            Vokuro\Models\Musics.title LIKE :q:
                        ")
                        ->orWhere("
                            Vokuro\Models\Users.name LIKE :q:
                        ")
                        ->orWhere("
                            Vokuro\Models\Finance.r_year LIKE :qY:
                        ")
                        ->orWhere("
                            Vokuro\Models\Finance.r_month LIKE :qM:
                        ")
                        ->orderBy('
                            Vokuro\Models\Finance.r_year DESC,
                            Vokuro\Models\Finance.r_month DESC
                        ')
                        ->getQuery()
                        ->execute(array(
                            'q'  => "%{$q}%",
                            'qY' => "{$qY}%",
                            'qM' => "%{$qM}"
                        ));
        } else {
            $builder = $this->modelsManager->createBuilder()
                        ->columns('
                            Vokuro\Models\Musics.code,
                            Vokuro\Models\Musics.title,
                            Vokuro\Models\Musics.member_id,
                            Vokuro\Models\Users.name,
                            Vokuro\Models\Finance.r_year,
                            Vokuro\Models\Finance.r_month
                        ')
                        ->from('Vokuro\Models\Musics')
                        ->join('Vokuro\Models\Finance', "Vokuro\Models\Musics.code = Vokuro\Models\Finance.id ")
                        ->join('Vokuro\Models\Users', "Vokuro\Models\Musics.member_id = Vokuro\Models\Users.id ")
                        ->where("
                            Vokuro\Models\Musics.member_id = {$this->auth->getIdentity()['id']}
                        ")
                        ->andWhere("
                            Vokuro\Models\Musics.code LIKE :q: OR
                            Vokuro\Models\Musics.title LIKE :q: OR
                            Vokuro\Models\Users.name LIKE :q: OR
                            Vokuro\Models\Finance.r_year LIKE :qY: OR
                            Vokuro\Models\Finance.r_month LIKE :qM:
                        ")
                        ->orderBy('
                            Vokuro\Models\Finance.r_year DESC,
                            Vokuro\Models\Finance.r_month DESC
                        ')
                        ->getQuery()
                        ->execute(array(
                            'q'  => "%{$q}%",
                            'qY' => "{$qY}%",
                            'qM' => "%{$qM}"
                        ));
        }

        $paginator = new Paginator(array(
            "data" => $builder,
            "limit" => 20,
            "page" => $numberPage
        ));

        $this->view->dataList = $paginator->getPaginate();
        $totalPages = array();
        for ( $idx = 1; $idx <= $paginator->getPaginate()->total_pages; $idx++)
        {
            $totalPages[] = $idx;
        }
        $this->view->totalPages = $totalPages;
        $this->view->q = $q;
    }

}
