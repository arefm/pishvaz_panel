<?php
namespace Vokuro\Controllers;

use Phalcon\Tag,
    Phalcon\Mvc\Model\Criteria,
    Phalcon\Paginator\Adapter\Model as Paginator,
    Vokuro\Forms\UploadForm,
    Vokuro\Models\Users,
    Vokuro\Models\Musics,
    Vokuro\Models\Finance,
    Tartan\XLSXReader;

/**
 * Vokuro\Controllers\UploadController
 * CRUD to manage profiles
 */
class UploadController extends ControllerBase
{

    /**
     * Default action. Set the private (authenticated) layout (layouts/private.volt)
     */
    public function initialize()
    {
        $this->view->setTemplateBefore('private');
    }

    public function indexAction()
    {
        $this->persistent->conditions = null;
        $this->view->form = new UploadForm();
        $this->view->userType = $this->auth->getUser()->profilesId;
    }

    public function uploadAction()
    {
        $this->view->userType = $this->auth->getUser()->profilesId;
        $userId = $this->auth->getIdentity()['memberId'];

        if ($this->request->hasFiles() == true)
        {
           $file = $this->request->getUploadedFiles()[0];
           $xlsx = new XLSXReader($file->getTempName());
           $sheetNames = $xlsx->getSheetNames();
           $sheetName = $sheetNames[1];
           $sheet = $xlsx->getSheet($sheetName);
           $data = $sheet->getData();

           $cnt = 0;
           foreach ($data as $key) {
               if($cnt > 0)
               {
                    $singerCode = $key[0];
                    $musicTitle = $key[1];
                    $musicCode  = $key[2];
                    $year       = $key[3];
                    $month      = $key[4];
                    $purchase   = $key[5];
                    $switchTo   = $key[6];
                    $switchFrom = $key[7];
                    $revenue    = $key[8];

                    $isExist    = Musics::findFirst("code = '" . $musicCode . "'");
                    if (!$isExist)
                    {
                        $saved = true;
                        $music = new Musics();
                        $music->assign(array(
                            'member_id' => $singerCode,
                            'title'     => $musicTitle,
                            'code'      => $musicCode,
                            'operator'  => $userId,
                            'date'      => (new \DateTime())->format("Y-m-d H:i:s")
                        ));
                        if (!$music->save()) 
                        {
                            $this->flash->error("Music: " . $music->getMessages()[0]);
                            $saved = false;
                        }

                        $finance = new Finance();
                        $finance->assign(array(
                            'id'         => $musicCode,
                            'r_year'     => $year,
                            'r_month'    => $month,
                            'purchase'   => $purchase,
                            'switch_to'  => $switchTo,
                            'switch_from'=> $switchFrom,
                            'revenue'    => $revenue,
                            'operator'   => $userId,
                            'date'       => (new \DateTime())->format("Y-m-d H:i:s")
                        ));
                        if (!$finance->save()) 
                        {
                            $this->flash->error("Finance: " . $finance->getMessages()[0]);
                            $saved = false;
                        }

                        if($saved)
                            $this->flash->success("Successfully saved " . $musicCode);

                    } else {
                        $music   = Musics::find("code = '" . $musicCode . "'");
                        $financeIsExist = Finance::find(array(
                            "id = {$musicCode} AND r_year = {$year} AND r_month = {$month}"
                        ));
                        if(count($financeIsExist) == 0){
                            $finance = new Finance();
                            $finance->assign(array(
                                'id'         => $musicCode,
                                'r_year'     => $year,
                                'r_month'    => $month,
                                'purchase'   => $purchase,
                                'switch_to'  => $switchTo,
                                'switch_from'=> $switchFrom,
                                'revenue'    => $revenue,
                                'operator'   => $userId,
                                'date'       => (new \DateTime())->format("Y-m-d H:i:s")
                            ));
                            if (!$finance->save()) 
                            {
                                $this->flash->error($finance->getMessages());
                            } else {
                                $this->flash->success("Successfully updated " . $musicCode);
                            }
                        } else {
                            $this->flash->error("Singer '({$music[0]->member_id})' already have this code ({$musicCode})");
                        }
                    }
               }
               $cnt++;
           }
           $destination = __DIR__ . "/../../uploads/" . date('YmdHis') . "_" . $userId . ".xlsx";
           $uploaded = $file->moveTo($destination);
        } else {
            return $this->dispatcher->forward(array(
                'action' => 'index'
            ));
        }
    }

}
