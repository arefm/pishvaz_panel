<?php
namespace Vokuro\Forms;

use Phalcon\Forms\Form,
    Phalcon\Forms\Element\Text,
    Phalcon\Validation\Validator\PresenceOf,
    Phalcon\Validation\Validator\StringLength as StringLength;

class SearchForm extends Form
{

    public function initialize($entity = null, $options = null)
    {
        $q = new Text('q', array(
            'placeholder' => 'جستجو برای'
        ));

        $q->addValidators(array(
            new PresenceOf(array(
                'message' => 'The search keyword is required'
            )),
            new StringLength(array(
                'min' => 2,
                'messageMinimum' => 'We want more than just their initials'
            ))
        ));

        $this->add($q);
    }
}
