<?php
namespace Vokuro\Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\File;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class UploadForm extends Form
{

    public function initialize($entity = null, $options = null)
    {
        $id = new Hidden('id');
        $this->add($id);

        $file = new File('file');

        $file->addValidators(array(
            new PresenceOf(array(
                'message' => 'File is required'
            ))
        ));

        $this->add($file);

    }
}
