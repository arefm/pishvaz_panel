<div class="navbar navbar-inverse">
  <div class="navbar-inner">
    <div class="container" style="width: auto;">
      <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      {{ link_to(null, 'class': 'brand', 'پنل کاربران')}}
        <div class="nav-collapse">
          <ul class="nav">
          
            {% if userType == 2 %}
              {%- set menus = [
                'ليست پيشواز': 'show',
                'آپلود': 'upload'
              ] -%}
            {% elseif userType == 1 %}
              {%- set menus = [
                'ليست پيشواز': 'show',
                'آپلود': 'upload',
                'اضافه کردن کاربر': 'session/signup'
              ] -%}
            {% else %}
              {%- set menus = [
                'ليست پيشواز': 'show'
              ] -%}
            {% endif %}

            {%- for key, value in menus %}
              {% if value == dispatcher.getControllerName() %}
              <li class="active">{{ link_to(value, key) }}</li>
              {% else %}
              <li>{{ link_to(value, key) }}</li>
              {% endif %}
            {%- endfor -%}

          </ul>

        <ul class="nav pull-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ auth.getName() }} <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li>{{ link_to('users/changePassword', 'تغيير رمز عبور') }}</li>
            </ul>
          </li>
          <li>{{ link_to('session/logout', 'خروج') }}</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<div class="container">
  {{ content() }}
</div>