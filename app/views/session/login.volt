{{ content() }}

<div align="center" class="well">

	{{ form('class': 'form-search') }}

	<div align="right">
		<h2>ورود کاربران</h2>
	</div>

		{{ form.render('memberId') }}
		{{ form.render('password') }}
		{{ form.render('go') }}

		<div align="center" class="remember">
			{{ form.render('remember') }}
			{{ form.label('remember') }}
		</div>

		{{ form.render('csrf', ['value': security.getToken()]) }}

	</form>

</div>