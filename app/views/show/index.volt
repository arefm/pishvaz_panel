<h1>ليست پيشواز</h1>
<p><br />
	<form method="post" action="{{ url("show/search") }}" autocomplete="off">
		<div class="center scaffold">
	        <div class="input-append"> 
	        	{{ form.render("q") }}
	            {{ submit_button("جستجو", "class": "btn") }}
	        </div>
    	</div>
	</form>
	<table class="table table-bordered table-striped" align="center">
	    <thead>
	        <tr>
	            <th>کد پيشواز</th>
	            <th>عنوان</th>
	            <th>خواننده</th>
	            <th>کد خواننده</th>
	            <th>ماه/سال</th>
	            <th>سفارش</th>
	            <th>آهنگ</th>
	        </tr>
	    </thead>
	    <tbody>
	    {%- for key in dataList.items %}
	        <tr>
	            <td>{{ key.code }}</td>
	            <td>{{ key.title }}</td>
	            <td>{{ key.name }}</td>
	            <td>{{ key.member_id }}</td>
	            <td>{{ key.r_year }}/{{ key.r_month }}</td>
	            <td>{{ key.orderTimes }}</td>
	            <td>
	            	<object type="application/x-shockwave-flash" data="dewplayer.swf?mp3=tones/{{ key.toneID }}.wav" width="200" height="20" id="dewplayer"><param name="wmode" value="transparent" /><param name="movie" value="dewplayer.swf?mp3=tones/{{ key.toneID }}.wav" /></object>
	            </td>
	        </tr>
	    {% endfor %}
	    	<tr>
	    		{% if dataList.total_pages > 0 %}
	    		<td colspan="7" style="text-align:center">
	    			{% if dataList.current == dataList.before %}
    					<span class="btn disabled">قبلی</span>
    				{% else %}
	    				{{ link_to("show?page=" ~ dataList.before, 'قبلی', "class": "btn ") }}
    				{% endif %}
	    			{%- for p in totalPages %}
	    				{% if dataList.current == p %}
	    					<span class="btn disabled">{{ p }}</span>
	    				{% else %}
	    					{{ link_to("show?page=" ~ p, p, "class": "btn ") }}
	    				{% endif %}
				    {% endfor %}
				    {% if dataList.current == dataList.next %}
    					<span class="btn disabled">بعدی</span>
    				{% else %}
	    				{{ link_to("show?page=" ~ dataList.next, 'بعدی', "class": "btn") }}
    				{% endif %}
	    		</td>
	    		{% else %}
	    		<td colspan="7" style="text-align:center">
	    		هيچ نتيجه ای يافت نشد!
	    		</td>
	    		{% endif %}
	    	</tr>
	    </tbody>
	</table>
</p>