<h4>جستجو برای "{{ q }}"</h4>
<p><br />
	<form method="post" action="{{ url("show/search") }}" autocomplete="off">
		<div class="center scaffold">
	        <div class="input-append"> 
	        	{{ form.render("q") }}
	            {{ submit_button("جستجو", "class": "btn") }}
	        </div>
    	</div>
	</form>
	<table class="table table-bordered table-striped" align="center">
	    <thead>
	        <tr>
	            <th>کد پيشواز</th>
	            <th>عنوان</th>
	            <th>خواننده</th>
	            <th>ماه/سال</th>
	        </tr>
	    </thead>
	    <tbody>
	    {%- for key in dataList.items %}
	        <tr>
	            <td>{{ key.code }}</td>
	            <td>{{ key.title }}</td>
	            <td>{{ key.name }}</td>
	            <td>{{ key.r_year }}/{{ key.r_month }}</td>
	        </tr>
	    {% endfor %}
	    	<tr>
	    		{% if dataList.total_pages > 0 %}
	    		<td colspan="4" style="text-align:center">
	    			{% if dataList.current == dataList.before %}
    					<span class="btn disabled">قبلی</span>
    				{% else %}
	    				{{ link_to("show/search?q=" ~ q|url_encode ~ "&page=" ~ dataList.before, 'قبلی', "class": "btn ") }}
    				{% endif %}
	    			{%- for p in totalPages %}
	    				{% if dataList.current == p %}
	    					<span class="btn disabled">{{ p }}</span>
	    				{% else %}
	    					{{ link_to("show/search?q=" ~ q|url_encode ~ "&page=" ~ p, p, "class": "btn ") }}
	    				{% endif %}
				    {% endfor %}
				    {% if dataList.current == dataList.next %}
    					<span class="btn disabled">بعدی</span>
    				{% else %}
	    				{{ link_to("show/search?q=" ~ q|url_encode ~ "&page=" ~ dataList.next, 'بعدی', "class": "btn") }}
    				{% endif %}
	    		</td>
	    		{% else %}
	    		<td colspan="4" style="text-align:center">
	    		هيچ نتيجه ای پيدا نشد!
	    		</td>
	    		{% endif %}
	    	</tr>
	    </tbody>
	</table>
</p>