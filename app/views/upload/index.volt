{{ content() }}

<form method="post" action="{{ url("upload/upload") }}" autocomplete="off" enctype="multipart/form-data">

    <div class="center scaffold">

        <h2>آپلود</h2>

        {{ form.render("id") }}
        <div class="clearfix">
            <label for="file">فايل مايکروسافت اکسل</label>
            {{ form.render("file") }}
        </div>

        <div class="clearfix">
            {{ submit_button("آپلود", "class": "btn btn-primary") }}
        </div>

    </div>

</form>