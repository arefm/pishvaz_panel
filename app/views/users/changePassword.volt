{{ content() }}

<form method="post" autocomplete="off" action="{{ url("users/changePassword") }}">

    <div class="center scaffold">

        <h2>تغيير کلمه عبور</h2>

        <div class="clearfix">
            <label for="password">کلمه عبور جديد</label>
            {{ form.render("password") }}
        </div>

        <div class="clearfix">
            <label for="confirmPassword">تاييد کلمه عبور جدید</label>
            {{ form.render("confirmPassword") }}
        </div>

        <div class="clearfix">
            {{ submit_button("Change Password", "class": "btn btn-primary", "value": "تغيير کلمه عبور") }}
        </div>

    </div>

</form>